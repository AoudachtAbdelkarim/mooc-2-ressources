- TP1 : introduction au HTML (2h)

Document ressource : introduction au HTML5. 

Lecture partielle. Présentation aux élèves.

Les élèves font un document bilan par groupe de 2 ou 3.

Cours suivant : un groupe passe au tableau pour présenter son travail + débat et questions pour élaborer un document final. Rapide.

A la fin du débat, introduire le DOM sur l'exercice 1 et celui partiel de la table de l'exercice 2

- TP2 : introduction au CSS (2h)

Documents ressources : introduction au HTML5 + introduction au CSS3. Lecture partielle. Présentation aux élèves.

Les élèves font un document bilan par groupe de 2 ou 3

Cours suivant : un groupe passe au tableau pour présenter son travail + débat et questions pour élaborer un document final. Rapide.

A la fin du débat, distribution du cours 


